script_name("vehicle-hit-notification")
script_author("THERION")

-- lib
local sampEvents = require("samp.events")
local bass = require("bass")
--

--- config
local SOUND_FILE = "probitie.mp3"
local SOUND_VOLUME = 1
local TIME_FOR_SHOT_TO_BE_COUNTED = 5
---

-- const
local BULLET_SYNC_TARGET_TYPES = {
   NOTHING = 0,
   PLAYER = 1,
   VEHICLE = 2,
}
--

-- state
local hitSound = nil
local lastVehicleYouShotAt = {
   time = 0,
   id = nil,
}
--

local function onSendBulletSync(targetType, targetId)
   if targetType ~= BULLET_SYNC_TARGET_TYPES.VEHICLE then
      return
   end

   local streamed, vehicle = sampGetCarHandleBySampVehicleId(targetId)
   if not streamed or getCarHealth(vehicle) < 250.0 then
      return
   end

   lastVehicleYouShotAt.time = os.clock()
   lastVehicleYouShotAt.id = targetId  
end

local function onInit()
   hitSound = bass.BASS_StreamCreateFile(false, "moonloader\\resource\\vehicle-hit-notification\\" .. SOUND_FILE, 0, 0, 0)
   bass.BASS_ChannelSetAttribute(hitSound, BASS_ATTRIB_VOL, SOUND_VOLUME)

   sampEvents.onSendBulletSync = function(data)
      onSendBulletSync(data.targetType, data.targetId)
   end
end

local function onEveryFrame()
   if not lastVehicleYouShotAt.id then
      return
   end
   if os.clock() > lastVehicleYouShotAt.time + TIME_FOR_SHOT_TO_BE_COUNTED then
      return
   end

   local streamed, vehicle = sampGetCarHandleBySampVehicleId(lastVehicleYouShotAt.id)
   if not streamed or getCarHealth(vehicle) >= 250.0 then
      return
   end

   bass.BASS_ChannelPlay(hitSound, true)
   lastVehicleYouShotAt.time = 0
   lastVehicleYouShotAt.id = nil
end

function main()
   repeat wait(0) until isSampAvailable()
   onInit()
   while true do
      wait(0)
      onEveryFrame()
   end
end
